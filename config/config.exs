import Config

config :logger, :console,
  level: :warn

config :nostrum,
  token: File.read!("token"),
  num_shards: :auto,
  gateway_intents: [:guild_integrations,:guild_messages,:guild_message_reactions]
