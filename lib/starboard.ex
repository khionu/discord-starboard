defmodule Starboard do
  @moduledoc """
  Documentation for `Starboard`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Starboard.hello()
      :world

  """
  def hello do
    :world
  end
end
