defmodule Starboard.Bot do
  use Nostrum.Consumer

  import Logger

  alias Nostrum.Api

  def start_link do
    Consumer.start_link(__MODULE__)
  end

  def handle_event({:MESSAGE_REACTION_ADD, reaction, _ws_state}) do
    

    Logger.error("Reaction payload: #{inspect reaction}")
  end

  def handle_event(event) do
    # Logger.error("Unhandled event: #{inspect event}")
    :noop
  end
end
