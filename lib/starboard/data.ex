defmodule Starboard.Data do
  use GenServer

  @impl true
  def init(_args) do
    case :ets.file2tab('data.ets') do
      {:error, _} -> init_ets()
      res -> res
    end
  end

  defp init_ets do
    :ets.new(:starb_data, [:named_table])

    if File.exists?("data.ets") do
      bak()
    end

    :ok = :ets.tab2file(:starb_data, 'data.ets', [{:sync, true}])

    :starb_data
  end

  defp bak(i \\ 0) do
    path = case i do
      0 -> "data.ets.bak"
      _ -> "data.ets.bak.#{i}"
    end

    case File.exists?(path) do
      true -> bak(i + 1)
      false -> File.rename!("data.ets", path)
    end
  end

  @impl true
  def handle_call({:update_config, guild_id, delta}, _from, tab) do
    key = {"config", guild_id}

    {new, old} = case :ets.lookup(tab, key) do
      [{_, config}] -> {Map.merge(config, delta), config}
      _ -> {delta, nil}
    end

    :ets.insert(tab, {key, new})

    {:reply, old, tab}
  end
end
